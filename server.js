let Pusher = require('pusher-js');
let notifier = require('node-notifier');
let env = require('dotenv');
let server = require('express')();

env.config();

let pusher = new Pusher('your-app-key', {
    cluster: 'us2',
    forceTLS: true
});

let envListeners = `${process.env.LISTENERS}`;
let listeners = envListeners.split(',');

server.get('*', (req, res) => {
    if (req.url == '/kill') {
        setTimeout(() => process.exit(), 500);
    }
});

for (let listener of listeners) {
    let info = listener.split('|');
    pusher.subscribe(info[0]).bind(info[1], function (payload) {
        notifier.notify({
            'title': payload.title,
            'message': payload.body,
            'wait': true,
            'icon': 'home.png'
        });
    })
}

server.listen(1560, function () {
    console.log('Webserver started.');
    console.log('http://localhost:1560/kill to terminate instance.');
});