<?php
/**
 * Demo of PHP implementation of Pusher
 */

require('vendor/autoload.php');

$pusher = new \Pusher\Pusher(
    'your-app-key',
    'your-app-secret',
    'your-app-id',
    [
        'cluster' => 'us2',
    ]
);

$pusher->trigger('test-channel', 'test-event', [
    'title' => 'Notification Title',
    'body' => 'Here is a notification send to pusher from PHP!'
]);